# Entity Link

Adds the following entity data properties to link items referencing entities:

* `entity_type_id`
* `entity_id`
* `entity_bundle`
* `entity_uuid`

Example usage:

```php
$menu_link_content = MenuLinkContent::load(1);
$linked_entity_uuid = $menu_link_content->link->entity_uuid
```
