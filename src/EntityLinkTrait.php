<?php

namespace Drupal\entity_link;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Url;

/**
 * Trait useful to fetch entity link metadata.
 */
trait EntityLinkTrait {

  /**
   * Returns the entity link metadata.
   *
   * @param string $uri
   *   The link URI.
   * @param \Drupal\Core\Url $url
   *   The link URL object.
   *
   * @return array|null
   *   The entity link metadata or NULL, if no entity is linked.
   */
  public function getLinkEntityMetadata(string $uri, Url $url): ?array {
    $metadata = NULL;

    if (str_starts_with($uri, 'entity:')) {
      $parameters = $url->getRouteParameters();
      $entity_type_id = key($parameters);
      $entity_id = reset($parameters);

      try {
        $entity = $this->getStorage($entity_type_id)->load($entity_id);
        if ($entity instanceof EntityInterface) {
          $metadata = [
            'entity_type_id' => $entity_type_id,
            'entity_id' => $entity->id(),
            'entity_bundle' => $entity->bundle(),
            'entity_uuid' => $entity->uuid(),
          ];
        }
      }
      catch (\Exception $e) {
        $this->logException($e);
      }
    }

    return $metadata;
  }


  /**
   * Logs the specified exception.
   *
   * @param \Exception $e
   *   An exception.
   */
  protected function logException(\Exception $e) {
    watchdog_exception('entity_link', $e);
  }

  /**
   * Returns the storage for the specified entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   An entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getStorage(string $entity_type_id): EntityStorageInterface {
    return \Drupal::entityTypeManager()->getStorage($entity_type_id);
  }

}
