<?php

namespace Drupal\entity_link\Entity;

use Drupal\entity_link\EntityLinkTrait;
use Drupal\menu_link_content\Entity\MenuLinkContent;

/**
 * Entity link-specific menu link content entity.
 */
class MenuEntityLinkContent extends MenuLinkContent {

  use EntityLinkTrait;

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinition() {
    $definition = parent::getPluginDefinition();

    $uri = $this->link->first()->uri;
    $url = $this->getUrlObject();
    if ($url->isRouted() && ($metadata = $this->getLinkEntityMetadata($uri, $url))) {
      $definition['metadata']['entity_link'] = $metadata;
    }

    return $definition;
  }

}
