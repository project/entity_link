<?php

namespace Drupal\entity_link\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as t;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\entity_link\EntityLinkTrait;
use Drupal\link\Plugin\Field\FieldType\LinkItem;

/**
 * Entity link plugin implementation of the 'link' field type.
 *
 * @property string $uri
 */
class EntityLinkItem extends LinkItem {

  use EntityLinkTrait;

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['entity_type_id'] = DataDefinition::create('string')
      ->setLabel(new t('Entity Type ID'))
      ->setComputed(TRUE)
      ->setInternal(FALSE);

    $properties['entity_id'] = DataDefinition::create('string')
      ->setLabel(new t('Entity ID'))
      ->setComputed(TRUE)
      ->setInternal(FALSE);

    $properties['entity_bundle'] = DataDefinition::create('string')
      ->setLabel(new t('Entity Bundle'))
      ->setComputed(TRUE)
      ->setInternal(FALSE);

    $properties['entity_uuid'] = DataDefinition::create('string')
      ->setLabel(new t('Entity UUID'))
      ->setComputed(TRUE)
      ->setInternal(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function get($property_name): TypedDataInterface {
    if (!array_key_exists($property_name, $this->values)) {
      $this->values[$property_name] = NULL;

      if (str_starts_with($property_name, 'entity_') && ($values = $this->getLinkEntityMetadata($this->uri, $this->getUrl()))) {
        foreach ($values as $name => $value) {
          $this->values[$name] = $value;
          if (isset($this->properties[$name])) {
            $this->properties[$name]->setValue($value, FALSE);
          }
        }
      }
    }

    return parent::get($property_name);
  }

}
